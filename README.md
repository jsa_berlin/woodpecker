
# before pushing you branch

  

please follow the steps in this [link](https://gist.github.com/santisbon/a1a60db1fb8eecd1beeacd986ae5d3ca) before pushing your feature branch, to ensure that your branch is up-to-date with current master

  

# Local Deployment

  

## Create and activate a virtual environment

  

* for Linux follow this [tutorial](https://hostadvice.com/how-to/how-to-create-a-virtual-environment-for-your-django-projects-using-virtualenv/)

* For windows follow this [tutorial](https://www.youtube.com/watch?v=N5vscPTWKOk&t=156s)

  

# install requirements

  

pip install -r requirements.txt

  

## Database settings

  

make sure that

  

* postgres is installed on your machine

* you have a password and a user name

* add your username and password to your environment variables 

	* on ubuntu create the file `/etc/profile.d/myenvvars.sh` with the following

		`export DB_USER="USER_NAME"`

		`export DB_PASS="PASSWORD"`
        
    then close all terminals

	* on windows follow [this tutorial](https://www.youtube.com/watch?v=IolxqkL7cD8)

  

* create a database with the name `woodpecker` as in the following [link](http://www.postgresqltutorial.com/postgresql-create-database/)

  

* From project root folder run migration using: `python manage.py migrate`

  

# Cloudinary

All photos are hosted on Cloudinary and therefore you need to create a free account, get the following variables from the console and then add them to your environment
`CLOUDINARY_CLOUD_NAME`

`CLOUDINARY_API_KEY`

`CLOUDINARY_API_SECRET`

  

## Runnding Celery and Redis
* From project root folder run: `celery -A photos.celery worker -l DEBUG -E`

  

*  `docker run -d -p 6379:6379 redis`

	* to run Redis on windows follow this [tutorial](https://www.techomoro.com/how-to-run-redis-on-windows-10/)

  

## running the server
`python manage.py runserver`
