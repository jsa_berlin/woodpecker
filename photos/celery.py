from __future__ import absolute_import
import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'woodpecker.settings') # DON'T FORGET TO CHANGE THIS ACCORDINGLY
os.environ.setdefault('FORKED_BY_MULTIPROCESSING', '1')

app = Celery('woodpecker')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()