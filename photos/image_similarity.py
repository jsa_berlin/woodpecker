import argparse
import os
import pathlib

import numpy as np
import keras
from keras import backend as k
import keras.applications as kapp
from PIL import Image, ExifTags
import io
import random
import scipy
import scipy.spatial

from collections import namedtuple
from jinja2 import Environment, FileSystemLoader

import random
import string

ImageFile = namedtuple("ImageFile", "src filename path uri")

def build_model(model_name):
    """ Create a pretrained model without the final classification layer. """
    if model_name == "resnet50":
        model = kapp.resnet50.ResNet50(weights="imagenet", include_top=False)
        return model, kapp.resnet50.preprocess_input
    elif model_name == "vgg16":
        model = kapp.vgg16.VGG16(weights="imagenet", include_top=False)
        return model, kapp.vgg16.preprocess_input
    else:
        raise Exception("Unsupported model error")

def fix_orientation(image):
    """ Look in the EXIF headers to see if this image should be rotated. """
    try:
        for orientation in ExifTags.TAGS.keys():
            if ExifTags.TAGS[orientation] == "Orientation":
                break
        exif = dict(image._getexif().items())

        if exif[orientation] == 3:
            image = image.rotate(180, expand=True)
        elif exif[orientation] == 6:
            image = image.rotate(270, expand=True)
        elif exif[orientation] == 8:
            image = image.rotate(90, expand=True)
        return image
    except (AttributeError, KeyError, IndexError):
        return image

def extract_center(image):
    """ Most of the models need a small square image. Extract it from the center of our image."""
    width, height = image.size
    new_width = new_height = min(width, height)

    left = (width - new_width) / 2
    top = (height - new_height) / 2
    right = (width + new_width) / 2
    bottom = (height + new_height) / 2

    return image.crop((left, top, right, bottom))

def process_images(file_names, preprocess_fn):
    """ Take a list of image filenames, load the images, rotate and extract the centers, 
    process the data and return an array with image data. """
    image_size = 224

    image_data = np.ndarray(shape=(len(file_names), image_size, image_size, 3))
    for i, ifn in enumerate(file_names):
        im = ifn.src
        im = fix_orientation(im)
        im = extract_center(im)
        im = im.resize((image_size, image_size))
        im = im.convert(mode="RGB")
        filename = os.path.join(ifn.path, ifn.filename + ".jpg")
        im.save(filename)
        image_data[i] = np.array(im)
    return preprocess_fn(image_data)

def generate_features(model, images):
    return model.predict(images)

def ensure_directory(directory):
    pathlib.Path(directory).mkdir(parents=True, exist_ok=True)

def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

def get_features(image):
    uniqe_id = randomString()
    output_dir = "./"
    image_output_dir = os.path.join(output_dir, "output_temp")
    query_image= "query_image.jpg"
    query_image_file_name = [query_image]
    model, preprocess_fn = build_model("resnet50")
    query_image_file = [
        ImageFile(image, "query_image_"+uniqe_id, image_output_dir, "images/query_image.jpg")
    ]
    ensure_directory(image_output_dir)
    query_image_data = process_images(query_image_file, preprocess_fn)
    query_image_features = generate_features(model, query_image_data)
    query_image_features = query_image_features.reshape(query_image_features.shape[0], -1)
    os.remove(os.getcwd()+"/output_temp/query_image_"+uniqe_id+".jpg")
    k.clear_session()
    return query_image_features

def get_distances(query_image_features, db_image_features): 
    distances = scipy.spatial.distance.cdist(query_image_features, db_image_features, "cosine")
    #distances = scipy.spatial.distance.cityblock(query_image_features, db_image_features, "cityblock")
    return distances[0][0]