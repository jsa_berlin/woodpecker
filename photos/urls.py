from django.urls import path
from . import views

urlpatterns = [
    path('', views.photo_list, name='photo_list'),
    path('view/<int:pk>', views.photo_view, name='photo_view'),
    path('new', views.photo_create, name='photo_new'),
    path('evaluate/<int:pk>', views.photo_evaluate, name='photo_evaluate'),
    path('evaluate_groups', views.photos_evaluate, name='photos_evaluate'),
    path('delete/<int:pk>', views.photo_delete, name='photo_delete'),
    path('delete_group', views.delete_group, name='photos_delete'),
]