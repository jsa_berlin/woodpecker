# Generated by Django 2.2.1 on 2019-07-01 12:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('photos', '0002_auto_20190627_0915'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='not_processable',
            field=models.BooleanField(default=False),
        ),
    ]
