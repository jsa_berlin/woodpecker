from django.db import models
from django.contrib.auth.models import User
from django.db import models
from cloudinary.models import CloudinaryField
import base64
import pickle

class Photo(models.Model):
    title = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    created_on = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    features_generated = models.BooleanField(default=False)
    not_processable = models.BooleanField(default=False)
    image = CloudinaryField('image', default=None)
    features = models.BinaryField(default=b'\x08')
    features_90 = models.BinaryField(default=b'\x08')
    features_180 = models.BinaryField(default=b'\x08')
    features_270 = models.BinaryField(default=b'\x08')
    
    def convert_features(self, features):
        queryPhotoBinaryFeatures = base64.b64decode(features)
        queryPhotoFeatures = pickle.loads(queryPhotoBinaryFeatures)
        return queryPhotoFeatures

    def all_features(self): 
        all_features = []
        all_features.append(self.convert_features(self.features))
        all_features.append(self.convert_features(self.features_90))
        all_features.append(self.convert_features(self.features_180))
        all_features.append(self.convert_features(self.features_270))
        return all_features 