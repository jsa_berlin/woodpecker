from django.shortcuts import render, get_object_or_404, redirect
from photos.models import Photo
from django.forms import ModelForm
from photos.image_similarity import * 
import pickle
import base64
import requests
from .forms import PhotoForm
from django.contrib.auth.decorators import login_required
from celery import shared_task
from django.http import HttpResponse

threshold = 0.685

@login_required
def photo_list(request, template_name='photos/photo_list.html'):
    photos = Photo.objects.all().filter(user=request.user.id)
    data = {}
    data['photos_list'] = photos
    return render(request, template_name, data)

@login_required
def photo_view(request, pk, template_name='photos/photo_detail.html'):
    photo= get_object_or_404(Photo, pk=pk)
    if photo.user != request.user:
        return redirect('photo_list')
    return render(request, template_name, {'object':photo})

@shared_task
def generate_features(url, degree):
    try: 
        image_data = requests.get(str(url)).content
        if degree == 0: 
            image = Image.open(io.BytesIO(image_data))
        else:
            image = Image.open(io.BytesIO(image_data)).rotate(degree)
        features = get_features(image)
        features_bytes = pickle.dumps(features)
        features_base64 = base64.b64encode(features_bytes)
        return features_base64
    except:
        print("Error while generating features for rotation {}".format(degree))
        return False
    return True

def convert_features_to_bytes(features):
    photo_features_bytes = base64.b64decode(features)
    photo_features = pickle.loads(photo_features_bytes)
    return photo_features

def generate_distances(request, pk):
    queryPhoto = Photo.objects.filter(pk=pk)[0]
    all_features = queryPhoto.all_features()
    list_distances = []
    photosList = Photo.objects.filter(user=request.user.id)
    for photo in photosList:
        if photo.id == pk or not photo.features_generated:
            continue
        photo_features =  convert_features_to_bytes(photo.features)
        photo_features_90 =  convert_features_to_bytes(photo.features_90)
        photo_features_180 =  convert_features_to_bytes(photo.features_180)
        photo_features_270 =  convert_features_to_bytes(photo.features_270)
        best_distance = 99999999999 #arbitrary number
        for feature in all_features:
            distance = []
            distance.append(get_distances(feature, photo_features))
            distance.append(get_distances(feature, photo_features_90))
            distance.append(get_distances(feature, photo_features_180))
            distance.append(get_distances(feature, photo_features_270))        
            if min(distance) < best_distance:
                best_distance = min(distance)
        if best_distance < threshold:
            list_distances.append([best_distance, photo])
    sorted_distances = sorted(list_distances, key=lambda x: x[0])
    return sorted_distances

def merge_lists_common_element(list_of_lists):
    merged_lists= list_of_lists.copy()
    for sub_list in merged_lists:
        temp_list=merged_lists.copy()
        temp_list.remove(sub_list)
        for temp_sub_list in temp_list:
            for e, element in enumerate(sub_list):
                if element in temp_sub_list:
                    merge = list(set(sub_list + temp_sub_list))
                    if sub_list in merged_lists:
                        merged_lists.remove(sub_list)
                    if temp_sub_list in merged_lists:
                        merged_lists.remove(temp_sub_list)        
                    merged_lists.insert(0, merge) 
                    temp_list=merged_lists.copy()
                    if sub_list in temp_list:
                        temp_list.remove(sub_list)
                    e=0 
                    sub_list = merge
                    continue
    return merged_lists

def images_grouping(request):
    similar_photos = []
    merged_similar_photos = []
    photosList = Photo.objects.filter(user=request.user.id).filter(features_generated=True)
    for i, photo in enumerate(photosList):
        temp= generate_distances(request, photo.id)
        for x in temp:  
            #if x[0] < threshold:
            if [photo.id, x[1].id] not in similar_photos:
                # print(photo.id, x[1].id, x[0])
                similar_photos.append([x[1].id,photo.id])
    merged_similar_photos = merge_lists_common_element(similar_photos)

    #adding independent photos
    all_grouped_photos=[]
    for list_ in merged_similar_photos:
        for element in list_:
            all_grouped_photos.append(element)
    for photo in photosList:
            if photo.id not in all_grouped_photos:
                merged_similar_photos.append([photo.id])

    return merged_similar_photos

@login_required
def photo_evaluate(request, pk, template_name='photos/photo_evaluate.html'):
    photo= get_object_or_404(Photo, pk=pk)
    if photo.user != request.user:
        return redirect('photo_list')
    distances = generate_distances(request, pk=pk)
    return render(request, template_name, {'queryPhoto':photo, 'simialarPhotos' : distances})   

@login_required
def photos_evaluate(request, template_name='photos/photos_evaluate.html'):
    photosGroups = images_grouping(request)
    for g_index, sub_group in enumerate(photosGroups):
        for p_index, photo_id in enumerate(sub_group):
            photosGroups[g_index][p_index] = Photo.objects.filter(pk=photo_id)[0]
    return render(request, template_name, {'photosGroups':photosGroups}) 

@login_required
def photo_create(request, template_name='photos/photo_form.html'):
    if request.method == 'POST':
        form = PhotoForm(request.POST, request.FILES)
        if form.is_valid():
            new_photo = Photo(image = request.FILES['image'])
            new_photo.user = request.user
            new_photo.save()
            generate_features_async.delay(new_photo.id)
    else:
        form = PhotoForm()
    return render(request, template_name, {
        'form': form
    })

def delete_group(request):
    photosList = request.POST.getlist('photos[]')
    try: 
        for photo_id in photosList:
            Photo.objects.filter(pk=photo_id)[0].delete()
        return HttpResponse('', status=200)
    except: 
        return HttpResponse('', status=500)

def photo_delete(request, pk, template_name='photos/photo_confirm_delete.html'):
    photo= get_object_or_404(Photo, pk=pk)
    if photo.user != request.user:
        return redirect('photo_list') 
    if request.method=='POST':
        photo.delete()
        return redirect('photo_list')
    return render(request, template_name, {'object':photo})


@shared_task
def generate_features_async(pk):
    queryPhoto = Photo.objects.filter(pk=pk)[0]
    if queryPhoto.features_generated:
        return
    try: 
        queryPhoto.features = generate_features(queryPhoto.image.url, 0)
        queryPhoto.features_90 = generate_features(queryPhoto.image.url, 90)
        queryPhoto.features_180 = generate_features(queryPhoto.image.url, 180)
        queryPhoto.features_270 =generate_features(queryPhoto.image.url, 270)
        queryPhoto.features_generated = True
        queryPhoto.save()
    except:
        queryPhoto.features = b'\x08'
        queryPhoto.features_90 = b'\x08'
        queryPhoto.features_180 = b'\x08'
        queryPhoto.features_270 = b'\x08'
        queryPhoto.features_generated = False
        queryPhoto.not_processable = True
        queryPhoto.save()
        print("Error while generating binary features")